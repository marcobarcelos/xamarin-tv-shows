﻿using System;
using Android.App;
using Android.Runtime;
using TvShowsApp.Dependencies;
using TvShowsApp.Droid.Dependencies;
using TvShowsApp.Services;

namespace TvShowsApp.Droid
{
	[Application]
	public class TvShowsApp : Application
	{
		public TvShowsApp(IntPtr handle, JniHandleOwnership jni)
			: base(handle, jni)
		{
			
		}


		public override void OnCreate()
		{
			base.OnCreate();

			DependencyService.Register<ISQLite>(new SQLiteAndroid());
		}
	}
}
