﻿using System;
using System.IO;
using SQLite;
using TvShowsApp.Dependencies;

namespace TvShowsApp.Droid.Dependencies
{
	public class SQLiteAndroid : ISQLite
	{
		const string DatabaseFilename = "database.db3";

		public SQLiteConnection GetConnection()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var path = Path.Combine(documentsPath, DatabaseFilename);

			return new SQLiteConnection(path);
		}
	}
}
