﻿using System;
using Android.Widget;

namespace TvShowsApp.Droid
{
	public class TvShowViewHolder : Java.Lang.Object
	{
		public TextView TitleTextView { get; set; }

		public TextView RatingTextView { get; set; }
	}
}
