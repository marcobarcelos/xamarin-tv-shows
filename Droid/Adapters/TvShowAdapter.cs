﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using TvShowsApp.Models;

namespace TvShowsApp.Droid.Adapters
{
	public class TvShowAdapter : BaseAdapter<TvShow>
	{
		private const char StarCharacter = '★';
		private readonly LayoutInflater layoutInflater;
		private readonly List<TvShow> tvShows;

		public TvShowAdapter(Context context, List<TvShow> tvShows)
		{
			layoutInflater = LayoutInflater.From(context);
			this.tvShows = tvShows;
		}

		public override TvShow this[int position]
		{
			get
			{
				return tvShows[position];
			}
		}

		public override int Count
		{
			get
			{
				return tvShows.Count;
			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = convertView;

			if (view == null)
			{
				view = layoutInflater.Inflate(Resource.Layout.TvShowItem, parent, false);

				var viewHolder = new TvShowViewHolder()
				{
					TitleTextView = view.FindViewById<TextView>(Resource.Id.titleTextView),
					RatingTextView = view.FindViewById<TextView>(Resource.Id.ratingTextView)
				};

				view.Tag = viewHolder;
			}

			var tvShow = tvShows[position];
			var holder = (TvShowViewHolder)view.Tag;

			holder.TitleTextView.Text = tvShow.Title;
			holder.RatingTextView.Text = new string(StarCharacter, tvShow.Rating);

			return view;
		}
	}
}
