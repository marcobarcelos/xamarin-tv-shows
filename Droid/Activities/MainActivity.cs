﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android;
using TvShowsApp.Controllers;
using TvShowsApp.Droid.Adapters;

namespace TvShowsApp.Droid.Activities
{
	[Activity(Label = "Tv Shows", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		private Button addButton;
		private ListView tvShowsListView;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			addButton = FindViewById<Button>(Resource.Id.addButton);
			tvShowsListView = FindViewById<ListView>(Resource.Id.tvShowsListView);

			addButton.Click += (sender, e) => {
				var intent = new Intent(this, typeof(AddTvShowActivity));
				StartActivity(intent);
			};
		}

		protected override void OnResume()
		{
			base.OnResume();

			PopulateTvShows();
		}

		private void PopulateTvShows()
		{
			var tvShowController = new TvShowController();
			var tvShows = tvShowController.TvShows;

			var tvShowAdapter = new TvShowAdapter(this, tvShows);
			tvShowsListView.Adapter = tvShowAdapter;
		}
	}
}

