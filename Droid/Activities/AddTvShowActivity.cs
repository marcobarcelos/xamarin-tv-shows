﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TvShowsApp.Controllers;
using TvShowsApp.Models;

namespace TvShowsApp.Droid.Activities
{
	[Activity(Label = "Add Tv Show")]
	public class AddTvShowActivity : Activity
	{
		private EditText titleEditText;
		private SeekBar ratingSeekBar;
		private Button addButton;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.AddTvShow);

			titleEditText = FindViewById<EditText>(Resource.Id.titleEditText);
			ratingSeekBar = FindViewById<SeekBar>(Resource.Id.ratingSeekBar);
			addButton = FindViewById<Button>(Resource.Id.addButton);

			addButton.Click += (sender, e) => {
				AddTvShow();
				this.Finish();
			};

			ShowBackButton();
		}

		private void AddTvShow()
		{
			var title = titleEditText.Text;
			var rating = ratingSeekBar.Progress;
			var tvShow = new TvShow() {
				Title = title,
				Rating = rating
			};

			var tvShowController = new TvShowController();
			tvShowController.AddTvShow(tvShow);
		}

		private void ShowBackButton()
		{
			this.ActionBar.SetDisplayHomeAsUpEnabled(true);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
				case Android.Resource.Id.Home:
					this.Finish();
					return true;

				default:
					return base.OnOptionsItemSelected(item);
			}
		}
	}
}
