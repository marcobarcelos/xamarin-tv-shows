﻿using System;
using System.Collections.Generic;
using SQLite;
using System.Linq;
using TvShowsApp.Services;
using TvShowsApp.Models;
using TvShowsApp.Dependencies;

namespace TvShowsApp.Data
{
	public class TvShowDatabase
	{
		private readonly SQLiteConnection connection;

		public TvShowDatabase()
		{
			var sqlite = DependencyService.Get<ISQLite>();
			connection = sqlite.GetConnection();
			connection.CreateTable<TvShow>();
		}

		public List<TvShow> GetTvShows()
		{
			return connection.Table<TvShow>().ToList();
		}

		public void AddTvShow(TvShow tvShow)
		{
			connection.Insert(tvShow);
		}
	}
}
