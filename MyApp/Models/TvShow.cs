﻿using System;

namespace TvShowsApp.Models
{
	public class TvShow
	{
		public string Title { get; set; }

		public int Rating { get; set; }
	}
}
