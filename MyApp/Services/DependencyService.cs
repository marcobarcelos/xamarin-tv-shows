﻿using System;
using System.Collections.Generic;

namespace TvShowsApp.Services
{
	/// <summary>
	/// Represents a simple dependency service (IoC).
	/// </summary>
	public static class DependencyService
	{
		/// <summary>
		/// The dependencies.
		/// </summary>
		private static readonly Dictionary<Type, object> dependencies = new Dictionary<Type, object>();

		/// <summary>
		/// Gets an dependency by type.
		/// </summary>
		/// <typeparam name="T">The dependency type.</typeparam>
		public static T Get<T>()
		{
			var type = typeof(T);

			if (dependencies.ContainsKey(type)) {
				var dependency = dependencies[type];
				return (T)dependency;
			}

			return default(T);
		}

		/// <summary>
		/// Registers the specified dependency.
		/// </summary>
		/// <param name="dependency">The dependency to be registered.</param>
		/// <typeparam name="T">The dependency type.</typeparam>
		public static void Register<T>(object dependency)
		{
			var type = typeof(T);
			dependencies[type] = dependency;
		}
	}
}