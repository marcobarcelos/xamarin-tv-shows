﻿using System;
using SQLite;

namespace TvShowsApp.Dependencies
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}
