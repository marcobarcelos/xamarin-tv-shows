﻿using System;
using System.Collections.Generic;
using TvShowsApp.Data;
using TvShowsApp.Models;

namespace TvShowsApp.Controllers
{
	public class TvShowController
	{
		private readonly TvShowDatabase database;

		public List<TvShow> TvShows {
			get {
				return database.GetTvShows();
			}
		}

		public TvShowController()
		{
			database = new TvShowDatabase();
		}

		public void AddTvShow(TvShow tvShow)
		{
			database.AddTvShow(tvShow);
		}
	}
}
